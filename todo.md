# To Do

## Front End

- [ ] re-design timeline page (vertical, legend, D3 v5?)
    - [ ] port path trimming logic from old timeline
    - [x] fix collision force or find another way to lay out. 
    - [x] add sources to the graph, in muted colours
    - [x] draw time axis on Y 
    - [x] link legend themes to topos page


- [ ] add individual pages for references or find a way to link back to where they are used.

- [x] add link to next and prev on node page


## Back End & Admin

- [ ] add date-created when adding content

- [ ] clean up repository
- [ ] back up database
- [ ] test if both neo4j and Flask can be hosted on the same VPS
- [ ] optimise images
