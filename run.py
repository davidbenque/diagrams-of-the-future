# -*- coding: utf-8 -*-
# @Author: davidbenque
# @Date:   2018-01-10 13:46:18
# @Last Modified by:   davidbenque
# @Last Modified time: 2018-03-11 14:46:49

from CTF_app import app
import os
from os import path

from livereload import Server

# watch all files for reload
extra_dirs = ['CTF_app',]
extra_files = extra_dirs[:]
for extra_dir in extra_dirs:
    for dirname, dirs, files in os.walk(extra_dir):
        for filename in files:
            filename = path.join(dirname, filename)
            if path.isfile(filename):
                extra_files.append(filename)

if __name__ == '__main__':
    #port = int(os.environ.get('PORT', 5000))
    #app.run(port=port, debug=True, extra_files=extra_files)

    app.debug = True
    app.extra_files = extra_files
    server = Server(app.wsgi_app)
    server.serve()
