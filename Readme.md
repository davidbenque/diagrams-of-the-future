# Diagrams of the Future

*Diagrams of the Future* is an ongoing research project into the diagrammatic nature of algorithmic prediction. From probability theory's "normal" distribution, to Big Data operationalised as vector spaces by machine learning systems, the production of predictions is diagrammatic all the way down. This project is a collection of attempts to diagram the future—to rationalise it as a coordinate space through geometrical operations—with the aim to question the foundations of the current algorithmic regime.

### See the website at [dotf.xyz](http://dotf.xyz)

This is a purpose built tool to visualise and edit a Neo4j graph database in the browser. `Artefact` nodes and their links are shown in the front-end visualisation, `Reference`, `Image` and `Quote` nodes are used to attach content to them. 

Typically images and quotes come out of a reference (a paper or book) an into an artefact in this pattern `(a:Reference)-[r:found_in]->(b:Image)-[l:attached]->(c:Artefact) ` but either type can also be attached directly to an `Artefact` node. Artefacts are linked together to reflect a variety of historical relationships across time (thematic, citations, evolution of a technology or device, and so on), these each have a different appearance in the front end. 

In the editor, the full graph is shown in the top part, and elements can be dragged to the 'workbench' area for editing. Node and relationship properties can be edited through simple bootstrap forms triggered by keyboard shortcuts. 

![editor screenshot](CTF_app/static/_screenshots/Screenshot-2018-3-16-editor.png)

![editor form screenshot](CTF_app/static/_screenshots/Screenshot-2018-03-16-editor-form.png)

Built with:

- [Neo4j](https://neo4j.com/) graph database
- [Flask](http://flask.pocoo.org/)
- [Py2neo](http://py2neo.org/v3/)
- [D3.js](https://d3js.org/) (v3)


Other librairies used:   
JS: [Moment.js](http://momentjs.com/) for dates, [Mousetrap](https://github.com/ccampbell/mousetrap) for keyboard shortcuts  
Python: [Mistune](https://github.com/lepture/mistune) for markdown parsing, [Boto3](https://github.com/boto/boto3) to store images on Amazon S3 



---

This project is part of [David Benqué](http://davidbenque.com/)'s PhD research in Information Experience Design at the [Royal College of Art](http://ied.rca.ac.uk/) in London.  This work is supported by [Microsoft Research Cambridge](https://www.microsoft.com/en-us/research/lab/microsoft-research-cambridge/) (UK) through its PhD Scholarship Programme.