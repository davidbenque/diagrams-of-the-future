/*
* @Author: davidbenque
* @Date:   2018-02-22 19:45:30
* @Last Modified by:   davidbenque
* @Last Modified time: 2018-03-11 20:01:43
*/
var img_path = "http://ctf-img.s3.eu-west-2.amazonaws.com/";

var load_content = function(id){
                // get the node content from flask
            $.ajax({
                type : 'GET',
                url : '/api/get_node_content/' + id
            })
            .done(function(data){
              var title = d3.select(".title-block");
              title.html("");
              title.html("<h2>"+data.year+" - "+data.title+"</h2>");

              var content = d3.select(".content-block");
              content.html("");

              var image_divs = content.selectAll('div')
                .data(data.images)
                .enter()
                .append("div")
                .attr("class", "image-block");

              image_divs.append("img")
                .attr("src", function(d){ return img_path + d.filename });

              image_divs
                .append("div")
                .attr("class", "caption")
                .html(function(d){ 
                  var source_str = "";
                  if(d.source != null){source_str = d.source};
                  return d.caption + " " + source_str});

              content.selectAll('blockquote')
                .data(data.quotes)
                .enter()
                .append("div")
                .attr("class", "quote-block")
                .html(function(d){ return d.text + " — " + d.source});

              content.append("div").html('<hr><span class="reftitle">References:</span><br>')

              content.selectAll("refs")
                .data(data.ref_list)
                .enter()
                .append("div")
                .attr("class", "ref")
                .html(function(d){ return d})

              $("#Content-Modal").modal("toggle");
            })
}