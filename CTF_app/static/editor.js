/*
* @Author: davidbenque
* @Date:   2018-01-24 16:05:04
* @Last Modified by:   davidbenque
* @Last Modified time: 2018-03-11 15:46:55
*/

var img_path = "http://ctf-img.s3.eu-west-2.amazonaws.com/";

// mouse tracking
var mouseX, mouseY;

$("body").mousemove(function(e) {
    mouseX = e.pageX - $('.editor').offset().left;
    mouseY = e.pageY - $('.editor').offset().top;
})


var delete_node, edit_node, edit_link, delete_link;

var data = d3.json("/api/get_all_data", function(json) {

    // Initial setup
        var width = 960,
           height = 950;

        var svg = d3.select('.editor').append('svg')
            .attr('width', width)
            .attr('height', height);

        // Border Line
        var chop_border_y = 400;

        svg.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", width)
            .attr("height", chop_border_y)
            .attr("class", "graph-panel")

        // X scale
        time_scale = d3.time.scale()
        .domain([
        moment("1650-01-01").valueOf(),
        moment("1800-01-01").valueOf(),
        moment("1900-01-01").valueOf(),
        moment("1945-01-01").valueOf(),
        moment("2000-01-01").valueOf(),
        moment().valueOf() // now
        ])
        .range([0, width/8, width/4, width/2, width/4*3, width]);

        // Initialise force layout
        var force = d3.layout.force()
            .size([width, 300])
            .linkDistance(10)
            .charge(-30)
            .gravity(0.05);

        force
            .nodes(json.nodes)
            .links(json.links)
            .on("tick", tick)
            .start();

    // Links
        var link = svg.selectAll(".link")
            .data(json.links, function(d){ return d.id })
            .enter().append("line")
            .attr("class", function(d){return "link " + d.label})
            .on("click", click_link);

    // Nodes and drag behaviour

        var node_size = 4; //size in the full graph view
        var node_size_stgd = 20; // size on the chopping board

        drag = force.drag()
            .on("dragstart", function(){
                d3.event.sourceEvent.stopPropagation();
                d3.select(this).classed("dragging", true);
            })
            .on("dragend", function(d){
                d3.select(this).classed("dragging", false);

                // stage nodes when dragged over the border
                if(d.y > chop_border_y){
                    d.fixed = true;
                    d.staged = true;
                    d3.select(this).classed("staged", true);
                    d3.select(this).select("circle").attr("r", node_size_stgd);
                    d3.select(this).select("text").text(function(d){
                            return node_label(d);
                        });
                } else {
                    // un-stage nodes when dragged back up
                    d.fixed = false;
                    d.staged = false;
                    d3.select(this).classed("staged", false);
                    d3.select(this).select("circle").attr("r", node_size);
                    d3.select(this).select("text").text("");
                };

                d3.selectAll(".link")
                    .attr("class", function(d){
                        // stage a link if both source and target nodes are staged
                        if (d.source.staged == true && d.target.staged == true){
                            d.staged = true;
                            return d3.select(this).attr("class") + " " + "staged";
                        } else {
                            d3.select(this).classed("staged", false);
                            return d3.select(this).attr("class");
                        }
                    })

            } );

        svg.selectAll("circle")
            .attr("r", function(d){
                if (d3.select(this).classed("staged") == true){
                    return node_size_stgd;
                } else { return node_size };
            })

        var node = svg.selectAll(".node")
            .data(json.nodes, function(d){ return d.id })
            .enter().append("g")
            .attr("class", "node")
            .on("click",click_node)
            .call(drag)
            .on("mouseover", function(d){
                var console_field = d3.select(".console");

                console_field.html("> " + d.properties.title)
                })
            .on("mouseout", function(d){
                  d3.select(".console").html(">");
            });

        node.classed("new", function(d) { return d.properties.title === "new"; });

        node.append("circle")
            .attr("r","4")
            .attr("class", function(d){ return d.label });

        node.append("text")
            .attr("x", 0)
            .attr("y", 0)
            .text("");

        function node_label(d){
          var date;
          // date for all nodes incl. artefacts
          d.properties.date ? date = d.properties.date.slice(0, 4) + " - " : date = "";

          // for reference nodes get the author too
          if (typeof d.properties.full_ref !== 'undefined') {
            regex = /.+?\(\d*\)/gm; // grab the author and date
            var found = d.properties.full_ref.match(regex);
            found? date = found[0] + " " : date = "";
          }


          return date + d.properties.title;
        };

    // Selection & Clicking

        var selected_node = null,
        selected_link = null;

        function click_node(d){
            //if (d3.event.defaultPrevented) return;
            if (linking == false && d.staged == true){
                    selected_node = d;
                    selected_link = null;
                    restart_force();
            } else if (linking == true){
                target_node = d;
                create_link();
            };
            d3.event.stopPropagation();
        };

        function click_link(d){
            //if (d3.event.defaultPrevented) return;
            if (d.staged == true && linking == false){
                selected_link = d;
                selected_node = null;
                restart_force();
            };
            d3.event.stopPropagation();
        };

        // de-select if background is clicked
        svg.on("click", function(){
            if (linking == false){
                selected_node = null;
                selected_link = null;
                restart_force();
            }
        });

    // Edit node/link

        // generate a form field for a property
        function make_field(parent, key, value){

            if(key === "file_name" && value === "no image"){
                var row = parent.append('div')
                    .attr("class", "form-group row");

                row.append("label")
                    .attr("class", "col-sm-2 col-form-label")
                    .attr("for", key + "-field")
                    .html(key);

                row.append("div").attr("class", "col-sm-10")
                    .append("input")
                    .attr("type", "file")
                    .attr("id", key + "-field")
                    .attr("class", "form-control-file")
                    .attr("name", "file")
                    .attr("value", value);
            } else {
                var row = parent.append('div')
                    .attr("class", "form-group row");

                row.append("label")
                    .attr("class", "col-sm-2 col-form-label")
                    .attr("for", key + "-field")
                    .html(key);

                row.append("div").attr("class", "col-sm-10")
                    .append("input")
                    .attr("type", "text")
                    .attr("id", key + "-field")
                    .attr("class", "form-control")
                    .attr("name", key)
                    .attr("value", value);
            };
        };

        // generate the edit form
        Mousetrap.bind('e', function(){
            if (selected_node === null && selected_link === null){return}
            // for nodes
            else if (selected_node != null){
                var form = d3.select("#edit-form");
                form.html("");
                $('#Alert').hide();

                var zot_form = d3.select('#zotero_form');
                zot_form.html("");

                d3.select("#label-field").attr("value", function(){return selected_node.label;});
                d3.select("#id-field").attr("value", function(){return selected_node.id;});

                // Zotero input for refrences
                if (selected_node.label === "Reference"){

                  var zot_row = zot_form.append("div").attr("class", "form-group row");
                  zot_row.append("div").attr("class", "col-sm-10")
                    .append("input")
                    .attr("type", "text")
                    .attr("id", "zotero-field")
                    .attr("class", "form-control")
                    .attr("name", "zotero-uri")
                    .attr("value", "Zotofill™");
                  zot_row.append("button")
                    .attr("class", "btn btn-outline-primary col-sm-1")
                    .attr("OnClick", "to_zotero()")
                    .html("🔍")
                }

                // show the image if it is there
                if (selected_node.label === "Image" && selected_node.properties.file_name != "no image"){
                    var img_row = form.append('div').attr("class", "row");
                    img_row.append("div").attr("class", "col-sm-2");
                    img_row.append("div").attr("class", "col-sm-8")
                        .append("img")
                        .attr("src", img_path + selected_node.properties.file_name)
                        .attr("class", "preview-img");
                    img_row.append("div").attr("class", "col-sm-2");
                }

                // create a form field for each property
                for (var key in selected_node.properties) {
                    if (selected_node.properties.hasOwnProperty(key)) {
                        make_field(form, key, selected_node.properties[key]);
                    }
                }
            // for links
            } else if (selected_link != null){
                var form = d3.select("#edit-form");
                form.html("");
                $('#Alert').hide();

                d3.select("#label-field").attr("value", function(){return selected_link.label;});
                d3.select("#id-field").attr("value", function(){return selected_link.id;});

                var source_target_row = form.append('div').attr("class", "form-group row")

                // make source and target fields (read only)
                // this horror might need to go
                source_target_row.append("label")
                    .attr("for", "source-field")
                    .attr("class", "col-sm-2 col-form-label")
                    .html("Source");

                source_target_row.append("div").attr("class", "col-sm-4")
                    .append("input")
                    .attr("type", "text")
                    .attr("class", "form-control")
                    .attr("name", "source")
                    .attr("id", "source-field")
                    .attr("value", selected_link.source.id)
                    .attr("readonly", true);

                source_target_row.append("label")
                    .attr("for", "target-field")
                    .attr("class", "col-sm-2 col-form-label")
                    .html("Target");

                source_target_row.append("div").attr("class", "col-sm-4")
                    .append("input")
                    .attr("type", "text")
                    .attr("class", "form-control")
                    .attr("name", "target")
                    .attr("id", "target-field")
                    .attr("value", selected_link.target.id)
                    .attr("readonly", true);

                for (var key in selected_link.properties) {
                    if (selected_link.properties.hasOwnProperty(key)) {
                        make_field(form, key, selected_link.properties[key]);
                    }
                }

            };

            $("#Edit-Form-Modal").modal("toggle");
        });


        // receive edited node/link data back form Flask

        edit_node = function(new_properties){
            var node_index = force.nodes().indexOf(selected_node);
            force.nodes()[node_index].properties = new_properties;
            restart_force();
        };

        edit_link = function(new_properties){
            var link_index = force.links().indexOf(selected_link);
            force.links()[link_index].properties = new_properties;
            restart_force();
        };

    // Create New Node

        function create_node(node_label){
            var new_node = {
                label: node_label,
                title: "new"
            };

            // initialise new nodes according to the schema
            if (node_label === 'Artefact'){
                new_node.date = "1950-01-01"
            } else if (node_label === 'Reference'){
                new_node.date = "1950-01-01",
                new_node.type = "book or mag",
                new_node.full_ref = "Author (year) title, publisher",
                new_node.quote_ref = "(author, year)"
            } else if (node_label === "Image"){
                new_node.file_name = "no image",
                new_node.caption = "caption string"
            } else if (node_label === "Quote"){
                new_node.text = "quote text"
            };

            // send new node to flask
            $.ajax({
                data : new_node,
                type : 'POST',
                url : '/editor/create_node'
            })
            .done(function(data){
                // receive new node back
                force.stop();
                var incoming_node = data.node;
                incoming_node.fixed = true;
                incoming_node.staged = true;
                incoming_node.x = mouseX;
                incoming_node.y = mouseY;
                force.nodes().push(incoming_node);
                restart_force();
            })
        };

        // new node keyboard bindings
        Mousetrap.bind('a', function(){
            create_node("Artefact")
        });

        Mousetrap.bind('r', function(){
            create_node("Reference")
        });

        Mousetrap.bind('q', function(){
            create_node("Quote")
        });

        Mousetrap.bind('i', function(){
            create_node("Image")
        });

    // Create new link
        var target_node = null;
        var linking = false;
        // line displayed when dragging new links
        var drag_line = svg.append("line")
            .attr("class", "drag-line hidden")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", 0)
            .attr("y2", 0);

        // keyboard binding to enter linking mode
        Mousetrap.bind('l', function(){
            if (selected_node === null){ return };
            linking = true;

            drag_line
                .attr("x1", selected_node.x)
                .attr("y1", selected_node.y)
                .attr("x2", mouseX)
                .attr("y2", mouseY)
                .attr("class", "drag-line visible");

            var mouse_coords = [0,0];

            svg.on("mousemove", function(){
                if(linking == true){
                    if(selected_node != null){
                        drag_line
                            .attr("x2", mouseX)
                            .attr("y2", mouseY);
                    };
                }
            });
            // this is exited by clicking on a target node (see click_node())
            // which calls create_link() below
        });

        function create_link(){
            linking = false;
            var do_it = true;
            drag_line
                .attr("x2", target_node.x)
                .attr("y2", target_node.y)
                .attr("class", "drag-line hidden");

            // check that the link doesn't already exist
            force.links().forEach(function(d){
                if (d.source === selected_node && d.target === target_node){
                    alert("This link already exists");
                    do_it = false;
                    return;
                } else if (d.source === target_node && d.target === selected_node){
                    alert("This links already exists in the opposite direction");
                    do_it = false;
                    return;
                }
            });

            // check that it is not linking a node to itself
            if (selected_node == target_node){
                alert("Can't link node to itself (well, can but won't)");
                do_it = false;
                return;
            };

            // break out if any of the above checks were positive
            if (do_it === false){ return };

            // new link is defined
            new_link = {
                source: selected_node.id,
                target: target_node.id
            };

            // choose link label according to schema
            if (selected_node.label === "Artefact" && target_node.label === "Artefact") {
                new_link.label = "related_to";
                new_link.by = "";
            } else if (selected_node.label === "Reference" && (target_node.label === "Image" || target_node.label === "Quote")) {
                new_link.label = "found_in";
                new_link.page = "";
            }  else  if (target_node.label === "Artefact") {
                new_link.label = "attached";
            } else {
                // default is 'to' like in Neo4j
                new_link.label = "to";
            };

            // send the new link to flask
            $.ajax({
                data : new_link,
                type : 'POST',
                url : '/editor/create_link'
            })
            .done(function(data){
                // get the response
                force.stop();
                var incoming_link = data.link;
                // match incoming ids with nodes in force.nodes()
                var new_source = force.nodes().find(function( obj ) { return obj.id === parseInt(incoming_link.source, 10); });
                var new_target = force.nodes().find(function( obj ) { return obj.id === parseInt(incoming_link.target, 10); });

                // define the link to be added to force.links()
                new_link = {
                    id: incoming_link.id,
                    label: incoming_link.label,
                    source: new_source,
                    target: new_target,
                    properties: incoming_link.properties
                }
                new_link.staged = true;
                force.links().push(new_link);
                restart_force();
            })
        };

    // Delete Node
        delete_node = function(id_to_delete){
            // detach all the links to the node
            // using a copy of the array for index lookup
            // deleting nodes (and their indexes) in a for loop caused problems
            force.links().slice(0).forEach(function(d){
                if(d.target == selected_node || d.source == selected_node){
                    ind = force.links().indexOf(d);
                    force.links().splice(ind,1);
                }
            });
            // delete the node itself
            sel_ind = force.nodes().indexOf(selected_node);
            force.nodes().splice(sel_ind, 1);
            restart_force();
        };

        delete_link = function(id_to_delete){
            sel_ind = force.links().indexOf(selected_link);
            force.links().splice(sel_ind, 1);
            restart_force();
        }

    // re-drawing function called after adding, editing, deleting nodes or links
    function restart_force(){

        // Links

        var link = svg.selectAll(".link")
            .data(force.links(), function(d){ return d.id });

        var new_links = link.enter().append("line")
            .attr("class", function(d){return "link staged " + d.label})
            .on("click", click_link);

        link.classed("selected", function(d) { return d === selected_link; });

        d3.selectAll(".link")
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

        link.exit().remove();

        // Nodes
        var node = svg.selectAll(".node")
        .data(force.nodes(), function(d){ return d.id });

        var new_nodes = node.enter().append("g")
            .attr("class", "node staged")
            .on("click",click_node);

        new_nodes.append("circle")
            .attr("r", 20)
            .attr("class", function(d){ return d.label });

        new_nodes.append("text")
            .attr("x", 0)
            .attr("y", 0)
            .text(function(d){
                return node_label(d);
            });

        node.call(drag);
        node.classed("selected", function(d) { return d === selected_node; });
        node.classed("new", function(d) { return d.properties.title === "new"; });

        // update the selected node after editing
        d3.select(".selected").select("text")
            .text(function(d){
                return node_label(d);
            });

        node.exit().remove();



        force.start();
    };

    // Executed on each tick of the force layout
    function tick() {
        d3.selectAll(".node").attr("transform", function(d) {
            var x_pos = 0;
            var y_pos = 0;

            if (d3.select(this).classed("dragging") == true ||
                d3.select(this).classed("staged") == true){
                x_pos = d.x;
                y_pos = d.y;
            } else if (d3.select(this).classed("dragging") == false){
                if (d.properties.date){
                    x_pos = time_scale(moment(d.properties.date).valueOf());
                    if (d.y > chop_border_y) { y_pos = chop_border_y }
                        else if (d.y <= 0) { y_pos = 0 }
                            else { y_pos = d.y};
                } else {
                    x_pos = d.x;
                    if (d.y > chop_border_y) { y_pos = chop_border_y }
                        else if (d.y <= 0) { y_pos = 0 }
                            else { y_pos = d.y};
                };
            };
            d.x = x_pos; // replace the x given by d3.force
            d.y = y_pos;
            return "translate(" + x_pos + "," + y_pos + ")"; });

        d3.selectAll(".link")
            .attr("x1", function(d) { return d.source.x; })
            .attr("y1", function(d) { return d.source.y; })
            .attr("x2", function(d) { return d.target.x; })
            .attr("y2", function(d) { return d.target.y; });

    };
});
