

Promise.all([d3.json('/api/timeline_legend')]).then(function(data){
    
    data = data[0];
    console.log(data.rel_types);
    leg_box = d3.select('.timeline_legend');

    var sparkline_w = 100;
    var sparkline_h = 15;

    function sparkline_path(){
        return "M 0," + sparkline_h / 2 + ", " + sparkline_w + "," + sparkline_h / 2;
    }

    leg_box.append("h1").attr("class", "legend").text('Legend');
    leg_box.append("h2").attr("class", "legend").text('Themes')

    leg_themes = leg_box.selectAll('div')
    .data(data.themes)
    .enter()
    .append("a")
    .attr('class', 'legend_item theme')
    .on("click", d => window.location = "/topos/"+ d.theme);


    leg_themes
        .append('svg')
        .attr("width", sparkline_w)
        .attr("height", sparkline_h)
        .attr("class", "sparkline")
        .append('path')
        .attr("class", function(d){ return "link theme " + d.theme })
        .attr("d", sparkline_path())
        .style("stroke", function(d) {
              return topoi_color(d.theme)
          });

    leg_themes
        .append('span')
        .html(d =>  '<i class="fa fa-external-link" aria-hidden="true"></i> ' + d.theme)

    leg_box.append("h2").attr("class", "legend").text('Links')

    leg_types = leg_box.selectAll('div.type')
    .data(data.rel_types)
    .enter()
    .append('div')
    .attr('class', 'legend_item type')


    leg_types.append('svg')
        .attr("width", sparkline_w)
        .attr("height", sparkline_h)
        .attr("class", "sparkline")
        .append('path')
        .attr("class", function(d){ return "link " + d.type })
        .attr("d", sparkline_path())
    
    leg_types.append('span').text(function(d){ return d.type})


});