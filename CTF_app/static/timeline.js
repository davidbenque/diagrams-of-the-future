var width = 800;
var height = 2000;


var time_ticks = [
  moment("1000-01-01", "YYYY-MM-DD"),
  moment("1800-01-01", "YYYY-MM-DD"),
  moment("1900-01-01", "YYYY-MM-DD"),
  moment("1945-01-01", "YYYY-MM-DD"),
  moment("2000-01-01", "YYYY-MM-DD"),
  moment() // now
  ];

var step = height / time_ticks.length;

var y_range = [
  height - 10,
  step * 5,
  step * 4,
  step * 3,
  step * 1.5,
  10
];

var y_timescale = d3.scaleTime()
.domain(time_ticks)
.range(y_range);

function timepos(d){
  var date = moment(d.properties.date, "YYYY-MM-DD");
  var timepos = y_timescale(date)
  return timepos;
};


var topoi_color = d3.scale.category10();


Promise.all([d3.json('/api/timeline')]).then(function(data){

  var data = data[0];
  console.log(data);
  svg = d3.select('.timeline').append('svg')
    .attr("width", width)
    .attr("height", height);

  marker_defs(svg);

  chartWrapper = svg.append('g').attr("class", "chartWrapper");

  var x_threads = d3.scaleBand()
    .domain(data.topoi)
    .range([170,width-50]);

    

  data.nodes.forEach( function(d){
    //d.idealX = width / 2;
    // d.idealX = d.label === "Artefact" ? width / 2.5 : width * 0.90;
     d.radius = d.label === "Artefact" ? 35 : 10;
  });

  var simulation = d3.forceSimulation(data.nodes)
    //.force('links', d3.forceLink(data.links).strength(2))
    .force('x', d3.forceX( d => x_threads(d.topos) ))
    .force('y', d3.forceY( d => timepos(d)).strength(10))
    //.force('center', d3.forceCenter(width / 2, height / 2))
    //.force('x', d3.forceManyBody().distanceMax(100).distanceMin(80))
    .force('collision', d3.forceCollide().radius(function(d){
      return d.radius;
    }))
    .stop();


  // run the simulation for a given number of ticks
    // See https://github.com/d3/d3-force/blob/master/README.md#simulation_tick
    for (var i = 0, n = 300; i < n; ++i) {
      simulation.tick();
    };

  // Y axis 

    yAxis = d3.axisLeft(y_timescale)
      .tickValues(time_ticks)
      .tickFormat(function(d){ return moment(d).format("YYYY"); })
      .tickSize(-width)

    chartWrapper.append('g').classed('y axis', true)
      .attr('transform', 'translate(50,0)')
      .call(yAxis);
        
  // Links
    chartWrapper.append("g")
      .attr("stroke", "#000")
      .attr("stroke-width", 1)
      .selectAll("line")
      .data(data.links)
      .enter().append("path")
      .attr("d", function(d){
        source = data.nodes[d.source];
        target = data.nodes[d.target]
        
        return "M" + source.x + "," + source.y + "," + target.x + "," + target.y;
      })
      .attr("class", function(d){
        var label = d.label;
        if (label === "ref_link"){
          return "link " + label;
        }
        var type = d.properties.by;
        if (type === "theme") {
          return "link theme " + d.properties.theme;
        } else {return "link " + type}
      })
      .style("stroke-width", function(d) { 
        return 1 + d.properties.strength; })
      .style("stroke", function(d) {
        if (d.properties.by === "theme") {
          return topoi_color(d.properties.theme)
        };
      });

  // Nodes
    

    var node_group = chartWrapper.selectAll(".node")
    .data(data.nodes)
    .enter().append("g")
    .attr("class", function(d){
      return "node " + d.label.toLowerCase()
    })
    .on("click", d => window.location = "/node/"+ d.id)
    .on("mouseover", d => console.log(d.topos, " ++ ", d.properties.date, d.properties.title))
    .on("mouseover", function(d){
      var console_field = d3.select(".console");
      console_field.attr("style", "background-color: white;");
      console_field.html("> " + d.properties.date.slice(0, 4) + ": " + d.properties.title)
      })
    .on("mouseout", function(d){
          d3.select(".console")
          .html(">")
          .attr("style", "background-color: none;");
    });

    // background circle
    node_group
      .append("circle")
      .attr("cx", function(d) { return d.x; })
      .attr("cy", function(d){ return d.y } )
      .attr("class", " artefact_node")
      .attr("r", 15);


});



