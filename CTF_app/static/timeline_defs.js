function marker_defs(svg){

    svg.append("svg:defs").selectAll("marker")
    .data(["translation"])      // Different link/path types can be defined here
  .enter().append("svg:marker")    // This section adds in the arrows
    .attr("id", function(d) { return d; })
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 10)
    .attr("refY", 0)
    .attr("markerWidth", 8)
    .attr("markerHeight", 8)
    .attr("orient", "auto")
  .append("svg:path")
    .attr("d", "M0,-5L10,0L0,5");
  
  svg.append("svg:defs").selectAll("marker")
    .data(["used_in"])      // Different link/path types can be defined here
  .enter().append("svg:marker")    // This section adds in the arrows
    .attr("id", function(d) { return d; })
    .attr("viewBox", "0 -5 10 10")
    .attr("refX", 10)
    .attr("refY", 0)
    .attr("markerWidth", 9)
    .attr("markerHeight", 9)
    .attr("orient", "auto")
  .append("svg:path")
    .attr("d", "M0,-5 L10,0 L0,5");
  
  svg.append("svg:defs")
    .append("svg:marker")
    .attr("id", "opposition-start")
    .attr("viewBox", "0 0 15 11")
    .attr("refX", 0)
    .attr("refY", 5)
    .attr("markerWidth", 25)
    .attr("markerHeight", 20)
    .attr("orient", "auto")
    .attr("markerUnits", "strokeWidth")
  .append("svg:path")
    .attr("d", "M5,0 L0,5 L5,10" +
                "M8,0 L3,5 L8,10" +
                "M11,0 L6,5 L11,10");
  
  svg.append("svg:defs")
    .append("svg:marker")
    .attr("id", "opposition-end")
    .attr("viewBox", "0 0 15 11")
    .attr("refX", 10)
    .attr("refY", 5)
    .attr("markerWidth", 25)
    .attr("markerHeight", 20)
    .attr("orient", "auto")
    .attr("markerUnits", "strokeWidth")
  .append("svg:path")
    .attr("d", "M0,0 L5,5 L0,10"+
               "M3,0 L8,5 L3,10" +
               "M6,0 L11,5 L6,10" );
  
  };