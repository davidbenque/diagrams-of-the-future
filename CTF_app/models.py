# -*- coding: utf-8 -*-
# @Author: davidbenque
# @Date:   2018-01-10 13:42:05
# @Last Modified by:   davidbenque
# @Last Modified time: 2018-03-13 11:31:55

from datetime import datetime
from random import shuffle

from py2neo import Graph, Node, Relationship, remote
from flask import json
from passlib.hash import bcrypt
import mistune
import boto3, botocore

from pyzotero import zotero
zotero = zotero.Zotero('2171240', 'user', 'Vu3f6jz57WnLb1Uo75UsGOW8')

from markdownify import markdownify as md
import re

from .config import NEO4J_USER, NEO4J_PASS, S3_KEY, S3_SECRET, S3_BUCKET, S3_LOCATION

# Connections

url = 'http://graph.countingthefuture.net:7474'
graph = Graph(url + '/db/data', username = NEO4J_USER, password=NEO4J_PASS)

s3 = boto3.client(
   "s3",
   aws_access_key_id=S3_KEY,
   aws_secret_access_key=S3_SECRET
)


# User

class User:
    def __init__(self, username):
        self.username = username

    def find(self):
        user = graph.find_one('User', 'username', self.username)
        return user

    def register(self, password):
        if not self.find():
            user = Node('User', username=self.username, password=bcrypt.encrypt(password))
            graph.create(user)
            return True
        else:
            return False

    def verify_password(self, password):
        user = self.find()
        if user:
            return bcrypt.verify(password, user['password'])
        else:
            return False


# API functions

def get_data():
    node_query='''
    match (n:Artefact)
    optional match (n)<-[r:attached]-(attachement)
    where (attachement:Image) or (attachement:Quote)
    with n, count(attachement) as nb_att
    RETURN ID(n), labels(n), properties(n), nb_att
    '''

    node_results = graph.run(node_query)
    nodes = []

    for node in node_results:
        node = {"id": node["ID(n)"],
            "label": node["labels(n)"][0], # assumes only 1 label
            "attachements": node["nb_att"],
            "properties": node["properties(n)"]}
        nodes.append(node)

    link_query='''
    MATCH (n:Artefact)-[r]->(m:Artefact)
    RETURN ID(r), type(r), ID(n), ID(m), properties(r)
    '''

    link_results = graph.run(link_query)
    links = []

    def node_index(node_id):
        return next((index for (index, d) in enumerate(nodes) if d["id"] == node_id), None)

    for link in link_results:
        link = {"id": link["ID(r)"],
            "label": link["type(r)"], # assumes only 1 label
            "source": node_index(link["ID(n)"]),
            "target": node_index(link["ID(m)"]),
            "properties": link["properties(r)"]}
        links.append(link)

    data = {"nodes": nodes, "links": links}
    return data


def get_timeline_data():
    
    nodes = []
    links = []

    def node_index(node_id):
        return next((index for (index, d) in enumerate(nodes) if d["id"] == node_id), None)

    node_query='''
    match (a:Artefact)
    optional match (a)-[l]-(b:Artefact) where l.by = "theme"
    RETURN ID(a), labels(a), properties(a), l.theme
    '''

    node_results = graph.run(node_query)

    topoi = []

    for node in node_results:
        topos = node["l.theme"] if node["l.theme"] else "base"
        topoi.append(topos)

        artefact = {"id": node["ID(a)"],
            "label": node["labels(a)"][0], # assumes only 1 label
            "properties": node["properties(a)"],
            "topos": topos}
        if artefact not in nodes:
            nodes.append(artefact)
        
    topoi = list(set(topoi))
    shuffle(topoi)
    topoi.insert(0, topoi.pop(topoi.index("base")))
    topoi.insert(0, "ref")

    # links between artefacts 
    link_query='''
    MATCH (n:Artefact)-[r]->(m:Artefact)
    RETURN ID(r), type(r), ID(n), ID(m), properties(r)
    '''

    link_results = graph.run(link_query)

    for link in link_results:
        link = {"id": link["ID(r)"],
            "label": link["type(r)"], # assumes only 1 label
            "source": node_index(link["ID(n)"]),
            "target": node_index(link["ID(m)"]),
            "properties": link["properties(r)"]}
        links.append(link)

    data = {"nodes": nodes, "links": links, "topoi": topoi}
    return data


def get_full_data():
    node_query = '''
    MATCH(n)
    RETURN ID(n), labels(n), properties(n)
    '''

    node_results = graph.run(node_query)
    nodes = []

    for node in node_results:
        node = {"id": node["ID(n)"],
            "label": node["labels(n)"][0], # assumes only 1 label
            "properties": node["properties(n)"]}
        nodes.append(node)

    link_query = '''
    MATCH (n)-[r]->(m)
    RETURN ID(r), type(r), ID(n), ID(m), properties(r)
    '''

    def node_index(node_id):
        return next((index for (index, d) in enumerate(nodes) if d["id"] == node_id), None)

    link_results = graph.run(link_query)
    links = []

    for link in link_results:
        link = {"id": link["ID(r)"],
            "label": link["type(r)"], # assumes only 1 label
            "source": node_index(link["ID(n)"]),
            "target": node_index(link["ID(m)"]),
            "properties": link["properties(r)"]}
        links.append(link)

    data = {"nodes": nodes, "links": links}
    return data


def get_image_data():
    image_query = '''
    MATCH (n:Image)-[:attached]->(m:Artefact)
    RETURN properties(n), ID(m)
    ORDER BY m.date DESC, n.title
    '''

    image_results = graph.run(image_query)
    images= []
    for image in image_results:
        props = image["properties(n)"]
        i = {
        "file": props["file_name"],
        "caption": props["caption"],
        "artefact_id": image["ID(m)"]
        }
        images.append(i)

    data = {"images": images}

    return data


def get_ref_data():
    ref_query = '''
    MATCH (n:Reference)
    RETURN properties(n)
    ORDER BY n.full_ref
    '''

    ref_results = graph.run(ref_query)
    books = []
    papers = []
    articles = []
    other = []
    primary = []
    for ref in ref_results:
        props = ref["properties(n)"]
        r = {
            "full_ref": mistune.markdown(props["full_ref"]),
            "type": props["type"]
        }
        if "link" in props:
            r["link"] = props["link"]

        if props["type"] == "book":
            books.append(r)
        elif (props["type"] == "paper"):
            papers.append(r)
        elif props["type"] == "article":
            articles.append(r)
        elif props["type"] == "primary":
            primary.append(r)
        else:
            other.append(r)

    data = {"books": books,
            "papers": papers,
            "articles": articles,
            "other": other,
            "primary": primary
            }

    return data


def get_node_content_data(node_id):

    query='''
        MATCH (n:Artefact)<-[:attached]-(a)
        WHERE ID(n)  = {id}
        OPTIONAL MATCH (a)<-[f]-(r:Reference)
        RETURN {
        node_title : n.title,
        node_date : n.date,
        type : labels(a),
        id : ID(a),
        props : properties(a),
        source_ref : r.full_ref,
        source : r.quote_ref ,
        source_link : r.link,
        source_page : f.page} as result
        ORDER BY ID(a)
        '''
    results = graph.run(query, id = int(node_id))

    node_props ={}
    images = []
    quotes = []
    ref_list = []

    for r in results:
        r = r["result"]
        att = {}
        node_props["title"] = r["node_title"]
        node_props["date"] = r["node_date"]
        props = r["props"]

        if r["type"][0] == "Image":
            att["type"] = "Image"
            att["filename"] = props["file_name"]
            att["caption"] = mistune.markdown(props["caption"], escape=False)
            images.append(att)
        elif r["type"][0] == "Quote":
            att["type"] = "Quote"
            att["text"] = mistune.markdown(props["text"], escape=False)
            quotes.append(att)
        elif r["type"][0] == "Reference":
            link = ""
            if "link" in props:
                link = " [[link]](" + props["link"] +")"
            ref_list.append(props["full_ref"] + link)

        if r["source"]:
            source = r["source"]
            if r["source_page"] != "":
                source = source[:-1] + ", p." + r["source_page"] + ")"
            att["source"] = source
            link = ""
            if r["source_link"]:
                link = " [[link]](" + r["source_link"] +")"
            ref_list.append(r["source_ref"] + link)

    ref_list = list(set(ref_list)) # remove duplicates
    ref_list.sort()
    ref_list = [mistune.markdown(ref) for ref in ref_list]

    node_content = {
        "title": node_props["title"],
        "year": node_props["date"][:4],
        "images": images,
        "quotes": quotes,
        "ref_list": ref_list
    }

    #previous and next nodes
    date_format = "%Y-%M-%d"
    this_date = datetime.strptime(node_props["date"], date_format)

    prevnext_query = '''
        MATCH (a)-[r]-(b:Artefact)
        WHERE ID(a) = {id}
        RETURN b.title as title, b.date as date, ID(b) as id, r.by as rel_type, r.theme as theme
        ORDER BY b.date DESC
    '''
    prevnexts = graph.run(prevnext_query, id = int(node_id)).data()

    prevs = []
    nexts = []

    for n in prevnexts:
        date = datetime.strptime(n["date"], date_format)

        if this_date > date:
            prevs.append(n)
        else:
            nexts.append(n)


    return {"content": node_content,
            "prev": prevs,
            "next": nexts
            }


def get_timeline_legend_data():
    rel_types_query = '''
        MATCH ()-[r:related_to]-()
        WHERE r.by <> "theme"
        RETURN DISTINCT r.by as type
    '''

    themes_query = '''
        match ()-[r:related_to]-()
        where r.by = "theme"
        return distinct r.theme as theme
    '''

    rel_types = graph.run(rel_types_query).data()
    themes = graph.run(themes_query).data()

    data = {"rel_types": rel_types, "themes": themes}
    return data


def get_landing_page_data():
    all_images = '''
        MATCH (a:Artefact)<-[r:attached]-(i:Image)
        return ID(a) as id, i.file_name as img_file, i.poster as poster
    '''
    nodes = '''
        MATCH (a:Artefact)
        RETURN ID(a) as id, a.title as title, a.date as date
        ORDER BY date DESC
    '''

    img_results = graph.run(all_images).data()
    node_results = graph.run(nodes).data()

    nodes=[]

    for n in node_results:
        id = n['id']

        node = {
            'id': id,
            'title': n['title'],
            'year': n['date'][:4],
        }

        images = [img for img in img_results if img.get('id') == id]
        if images:
            poster = [p for p in images if p.get('poster') == 1]
            if poster:
                poster = poster[0]
            else:
                shuffle(images)
                poster = images[0]
            node['image'] = poster['img_file']

            nodes.append(node)

    return nodes


def get_topos(topos_title):
    all_images = '''
        MATCH (a:Artefact)<-[r:attached]-(i:Image)
        return ID(a) as id, i.file_name as img_file, i.poster as poster
    '''
    nodes = '''
        MATCH (a:Artefact)-[r:related_to]-(b:Artefact)
        WHERE r.theme="{topos_title}"
        RETURN ID(a) as id, a.title as title, a.date as date
        ORDER BY date DESC
    '''

    img_results = graph.run(all_images).data()
    node_results = graph.run(nodes.format(topos_title=topos_title)).data()

    nodes = []

    for n in node_results:
        id = n['id']

        node = {
            'id': id,
            'title': n['title'],
            'year': n['date'][:4],
        }

        images = [img for img in img_results if img.get('id') == id]
        if images:
            poster = [p for p in images if p.get('poster') == 1]
            if poster:
                poster = poster[0]
            else:
                shuffle(images)
                poster = images[0]
            node['image'] = poster['img_file']
            
            if node not in nodes:
                nodes.append(node)
    return nodes

# Editor functions

def create_link(new_node_data):
    s = graph.node(int(new_node_data["source_id"]))
    t = graph.node(int(new_node_data["target_id"]))
    relationship = Relationship(s, new_node_data["label"], t, **new_node_data["properties"])
    graph.merge(relationship)
    response = {
        "label": relationship.type(),
        "id": remote(relationship)._id,
        "source": new_node_data["source_id"],
        "target": new_node_data["target_id"],
        "properties": new_node_data["properties"]
    }
    return json.jsonify({"link": response})

def edit_link(link_data):
    query = '''
        MATCH ()-[r]-() WHERE ID(r)= {id}
        SET r = {properties}
        '''
    properties = link_data["properties"]
    graph.run(query, id = int(link_data["id"]), properties = properties)

    message = "link {0} updated".format(link_data["id"])
    return json.jsonify({
        "message": message,
        "type": "link",
        "data": link_data["properties"]})

def delete_link(link_id):
    query = '''
        MATCH ()-[r]-() WHERE ID(r)={id}
        DELETE r
        '''
    graph.run(query, id = int(link_id))
    message = "link {0} deleted".format(str(link_id))
    return json.jsonify({
        "message": message,
        "type": "link",
        "data": link_id})

def create_node(new_node):
    node = Node(new_node["label"], **new_node["properties"])
    graph.create(node)
    response = {
        "label": new_node["label"],
        "id": remote(node)._id,
        "properties": dict(node)
    }

    return json.jsonify({"node":response})

def edit_node(node_data):
    query = '''
        MATCH (n) WHERE ID(n)= {id}
        SET n = {properties}
        '''
    properties = node_data["properties"]
    graph.run(query, id = int(node_data["id"]), properties = properties)

    message = "node {0} updated".format(node_data["id"])
    return json.jsonify({
        "message": message,
        "type": "node",
        "data": node_data["properties"]})

def delete_node(node_id):
    query = '''
    MATCH(n) WHERE ID(n)={0}
    DETACH DELETE n
    '''
    query = query.format(str(node_id))
    graph.run(query)
    message = "node {0} deleted".format(str(node_id))
    return json.jsonify({
        "message": message,
        "type":"node",
        "data": node_id})

def zotofill(id):

    item = zotero.item(id, format='json', include='citation,bib,data', style='harvard-cite-them-right')

    full_ref = md(item['bib'])
    full_ref = re.sub(r'https?:\/\/.*[\r\n]*', '', full_ref) # remove url
    full_ref = full_ref.replace('Retrieved from', '')

    return json.jsonify({
        "citation": md(item['citation']),
        "full_ref": full_ref,
        "date": '⚠️ ' + item['data']['date'],
        "title": item['data']['title'],
        "link": item['data']['url']
    })


def upload_to_s3(file, acl="public-read"):

    try:
        s3.upload_fileobj(
            file,
            S3_BUCKET,
            file.filename,
            ExtraArgs={
                "ACL": acl,
                "ContentType": file.content_type
            }
        )

    except Exception as e:
        # This is a catch all exception, edit this part to fit your needs.
        print("Something Happened: ", e)
        return e
