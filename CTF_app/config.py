# -*- coding: utf-8 -*-
# @Author: davidbenque
# @Date:   2018-03-11 14:53:45
# @Last Modified by:   davidbenque
# @Last Modified time: 2018-03-14 19:36:27

import os

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
PAGES_DIR = os.path.join(APP_ROOT, 'pages')

SECRET_KEY      = os.urandom(32)

NEO4J_USER      = os.environ.get("CTF_NEO4J_USER")
NEO4J_PASS      = os.environ.get("CTF_NEO4J_PASS")

S3_BUCKET       = os.environ.get("CTF_S3_BUCKET")
S3_KEY          = os.environ.get("CTF_S3_ACCESS")
S3_SECRET       = os.environ.get("CTF_S3_SECRET")
S3_LOCATION     = 'http://{}.s3.amazonaws.com/'.format(S3_BUCKET)