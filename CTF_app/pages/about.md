*Diagrams of the Future* is an ongoing research project into the diagrammatic nature of algorithmic prediction. From probability theory's "normal" distribution, to Big Data operationalised as vector spaces by machine learning systems, the production of predictions is diagrammatic all the way down. This project is a collection of attempts to diagram the future—to rationalise it as a coordinate space through geometrical operations—with the aim to question the foundations of the current algorithmic regime. 

[Minimum Viable Product](https://medium.com/@davidbenque/counting-the-future-8f593b826a13) presented at the 4S/EASST conference in Sept. 2016  
[The Flower and the Future](https://theairpump.davidbenque.com/the-flower-and-the-future/) presented at *This Happened London* in March 2018

This is a work in progress  
[Get in touch](mailto:david.benque@network.rca.ac.uk) with any feedback or comments.   
[Gitlab](https://gitlab.com/davidbenque/diagrams-of-the-future)




>This project is part of [David Benqué](http://davidbenque.com/)'s PhD research in Information Experience Design at the [Royal College of Art](http://ied.rca.ac.uk/) in London.  This work is supported by [Microsoft Research Cambridge](https://www.microsoft.com/en-us/research/lab/microsoft-research-cambridge/) (UK) through its PhD Scholarship Programme.
