# -*- coding: utf-8 -*-
# @Author: davidbenque
# @Date:   2018-01-10 13:42:21
# @Last Modified by:   davidbenque
# @Last Modified time: 2018-03-14 21:54:27

from .models import *

from flask import Flask, render_template, json, request, url_for, session, flash, redirect
from werkzeug.utils import secure_filename
import os
from os.path import join, dirname, realpath
from functools import wraps
import mistune

from .config import PAGES_DIR


app = Flask(__name__)

# Users - uncomment this to open regstrations

# @app.route('/register', methods=['GET','POST'])
# def register():
#     if request.method == 'POST':
#         username = request.form['username']
#         password = request.form['password']

#         if len(username) < 1:
#             flash('Your username must be at least one character.')
#         elif len(password) < 5:
#             flash('Your password must be at least 5 characters.')
#         elif not User(username).register(password):
#             flash('A user with that username already exists.')
#         else:
#             session['username'] = username
#             flash('Logged in.')
#             return redirect(url_for('index'))

#     return render_template('register.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if not User(username).verify_password(password):
            flash('Invalid login.')
        else:
            session['username'] = username
            flash('Logged in.')
            return redirect(url_for('editor'))

    return render_template('login.html')


@app.route('/logout')
def logout():
    session.pop('username', None)
    flash('Logged out.')
    return redirect(url_for('index'))


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        username = session.get('username')
        if not username:
            return redirect(url_for('login', next=request.url))
        return f(*args, **kwargs)
    return decorated_function


# Static Pages
import io #fix for python 2.x server

@app.route('/pages/<page_name>')
def page(page_name):

    try:
        f = os.path.join(PAGES_DIR, page_name + '.md')
        file = io.open(f, mode='r', encoding = "utf-8").read()
        content = mistune.markdown(file, escape=False)
    except:
        content = "This page does not exist."

    return render_template("pages.html", page_content = content)

# Files

UPLOAD_FOLDER = join(dirname(realpath(__file__)), 'static/_images/')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Pages

@app.route('/')
def landing_page():
    data = get_landing_page_data()
    return render_template("index.html", data=data)

@app.route('/images')
def image_browse():
    return render_template("images.html")

@app.route('/timeline')
def timeline():
    return render_template('timeline.html')

@app.route('/node/<node_id>')
def node(node_id):
    data = get_node_content_data(node_id)
    return render_template('node.html', data=data)

@app.route('/topos/<topos_title>')
def topos(topos_title):
    data = get_topos(topos_title)
    return render_template('topos.html', data=data, topos=topos_title)

@app.route('/editor')
@login_required
def editor():
    return render_template("editor.html")

@app.route('/references')
def ref_browse():
    data = get_ref_data()
    return render_template("references.html", data=data)

# API calls to get data

## old timeline with only artefacts
@app.route('/get_artefact_data')
def get_artefact_data():
    data = get_data()
    return json.jsonify(data)


@app.route('/api/timeline')
def get_timeline():
    data = get_timeline_data()
    return json.jsonify(data)

@app.route('/api/timeline_legend')
def get_timeline_legend():
    data = get_timeline_legend_data()
    return json.jsonify(data)

@app.route('/api/get_all_data')
def get_all_data():
    data = get_full_data()
    return json.jsonify(data)

@app.route('/api/get_images')
def get_images():
    data = get_image_data()
    return json.jsonify(data)

@app.route('/api/get_refs')
def get_refs():
    data = get_ref_data()
    return json.jsonify(data)

@app.route('/api/get_node_content/<node_id>')
def get_node_content(node_id):
    data = get_node_content_data(node_id)
    return json.jsonify(data)

# Editor

@app.route('/editor/create_node', methods=['POST'])
@login_required
def editor_create_node():
    if request.method == 'POST':
        data = request.form.to_dict()
        label = data.pop("label")
        new_node = {"label": label, "properties": data}
        return create_node(new_node)

@app.route('/editor/create_link', methods=['POST'])
@login_required
def editor_create_link():
    if request.method == 'POST':
        data = request.form.to_dict()
        new_node_data = {
            "label": data.pop("label"),
            "source_id": data.pop("source"),
            "target_id": data.pop("target"),
            "properties": data
        }
        return create_link(new_node_data)

@app.route('/editor/edit', methods=['POST'])
@login_required
def editor_edit():
    if request.method == 'POST':

        # check for file in image nodes
        filename = False
        if request.files:
            file = request.files['file']
            if file.filename == '':
                return json.jsonify({"message": "no file"})
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                file.filename = filename
                upload_to_s3(file)

        # Process node/link
        data = request.form.to_dict()
        label = data.pop("label")
        ID = data.pop("id")

        new_name = data.pop("newprop_name")
        new_value = data.pop("newprop_value")

        if "source" in data: # if it is a link
            link_data = {
                "label": label,
                "id": ID,
                "source": data.pop("source"),
                "target": data.pop("target"),
                "properties": data
            }
            if new_name and new_value != "":
                link_data["properties"][new_name] = new_value

            return edit_link(link_data)

        else: # it is a node
            node_data = {
                "label": label,
                "id": ID,
                "properties": data
            }
            # add new property if there is one
            if new_name and new_value != "":
                node_data["properties"][new_name] = new_value
            # add file_name property if a file has been provided
            if filename:
                node_data["properties"]["file_name"]= filename

            return edit_node(node_data)

@app.route('/editor/delete', methods=['POST'])
@login_required
def editor_delete():
    if request.method == 'POST':
        data = request.form.to_dict()
        if "source" in data: # is it a link
            return delete_link(data["id"])
        else:
            return delete_node(data["id"])

# autofill ref form with Zotero API
@app.route('/editor/zotofill/<zot_id>')
@login_required
def editor_zotofill(zot_id):
    return zotofill(zot_id)
