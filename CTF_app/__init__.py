# -*- coding: utf-8 -*-
# @Author: davidbenque
# @Date:   2018-01-10 13:42:41
# @Last Modified by:   davidbenque
# @Last Modified time: 2018-03-11 15:12:48
import os
from .views import app
from .models import graph

# examples from Nicole White tutorial
#graph.schema.create_uniqueness_constraint("User", "username")
#graph.schema.create_uniqueness_constraint("Tag", "name")
#graph.schema.create_uniqueness_constraint("Post", "id")

app.config.from_object("CTF_app.config")
