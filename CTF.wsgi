#!/usr/bin/python

import sys
import logging
logging.basicConfig(stream=sys.stderr)
sys.path.insert(0,"/var/www/countingthefuture.net/")
sys.path.insert(0,"/var/env/ctf")

import os


def application(environ, start_response):
    for key in ['CTF_NEO4J_USER', 'CTF_NEO4J_PASS', 'CTF_S3_BUCKET', 'CTF_S3_ACCESS', 'CTF_S3_SECRET']:
        os.environ[key] = environ.get(key, '')
    from CTF_app import app as _application
    return _application(environ, start_response)
