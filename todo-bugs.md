# Todo - bugs



## front-end graph


+ on the reference page; find a way to list the artefacts in which the ref is used, and open the content from there

+ code different relationship styles
  + Transpose not translate


+ "Now nodes" that anchor a continuing/still existing artefact to the right of the graph

  eg. the bloomberg terminal

+ URL for each node - provides link to copy so that you open hte timeline page with a the modal toggled and that node loaded

  `countingthefuture.com/13`


+ different `linkDistance` for types of links

  ``` javascript
    // Here's the part where we make the two graphs differ. Because
    // we're looking at the `linkDistance` property, that's what we
    // want to vary between the graphs. Most often this property is
    // set to a constant value for an entire visualization, but D3
    // also lets us define it as a function. When we do that, we
    // can set a different value for each link.

    force.linkDistance(function(link) {
       return link.graph === 0 ? height/2 : height/4;
    });
  ```

  from [here](http://jsdatav.is/visuals.html?id=83515b77c2764837aac2)

## editor

### Bugs

- nodes with dates outside the range disapear, limit the bounds
- new links are drawn on top of everything
- replace text input with textarea for quotes, and maybe also full_ref fields
- if a ref node is attached directly to an artefact, it breaks things if it doesn’t have a link
